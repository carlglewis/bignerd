//
//  AppDelegate.m
//  Homepwner
//
//  Created by Lewis, Carl on 10/25/16.
//  Copyright © 2016 Lewis, Carl. All rights reserved.
//

#import "AppDelegate.h"
#import "ItemStore.h"
#import "ItemsViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {

    ItemStore *itemStore = [ItemStore new];
    ItemsViewController *ivc = (ItemsViewController *)self.window.rootViewController;
    ivc.itemStore = itemStore;
    return YES;

}

- (void)applicationWillResignActive:(UIApplication *)application {

}

- (void)applicationDidEnterBackground:(UIApplication *)application {

}

- (void)applicationWillEnterForeground:(UIApplication *)application {

}

- (void)applicationDidBecomeActive:(UIApplication *)application {

}

- (void)applicationWillTerminate:(UIApplication *)application {

}

@end
