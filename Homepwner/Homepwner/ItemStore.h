//
//  ItemStore.h
//  Homepwner
//
//  Created by Lewis, Carl on 10/25/16.
//  Copyright © 2016 Lewis, Carl. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Item;

@interface ItemStore : NSObject

- (NSArray *)allItems;
- (Item *)createItem;

@end
