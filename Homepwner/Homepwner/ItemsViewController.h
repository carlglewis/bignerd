//
//  ItemsViewController.h
//  Homepwner
//
//  Created by Lewis, Carl on 10/25/16.
//  Copyright © 2016 Lewis, Carl. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ItemStore;

@interface ItemsViewController : UITableViewController

@property (nonatomic) ItemStore *itemStore;

@end
