//
//  AppDelegate.h
//  WorldTrotter
//
//  Created by Lewis, Carl on 10/24/16.
//  Copyright © 2016 Lewis, Carl. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

