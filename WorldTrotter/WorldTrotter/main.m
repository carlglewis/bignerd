//
//  main.m
//  WorldTrotter
//
//  Created by Lewis, Carl on 10/24/16.
//  Copyright © 2016 Lewis, Carl. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
