//
//  ViewController.m
//  QuizApp
//
//  Created by Lewis, Carl on 10/24/16.
//  Copyright © 2016 Lewis, Carl. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (nonatomic) NSArray *questions;
@property (nonatomic) NSArray *answers;
@property (nonatomic) NSInteger currentIndex;
@property (weak, nonatomic) IBOutlet UILabel* questionLabel;
@property (weak, nonatomic) IBOutlet UILabel* answerLabel;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.currentIndex = 0;
    self.questions = @[ @"What is 2 + 2",
                    @"What is 1 + 1?",
                    @"What is the capital of Georgia?"];
    
    self.answers = @[ @"4",
                      @"2",
                      @"Atlanta"];
    
    self.questionLabel.text = self.questions[self.currentIndex];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction) showNextQuestion: (UIButton *) sender {
    self.currentIndex = (self.currentIndex + 1) % self.questions.count;
    self.questionLabel.text = self.questions[self.currentIndex];
    self.answerLabel.text  = @"???";
}

- (IBAction) showAnswer: (UIButton *) sender {
    self.answerLabel.text = self.answers[self.currentIndex];
}


@end
